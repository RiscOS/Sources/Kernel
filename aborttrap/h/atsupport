/*
 * Copyright (c) 2021, RISC OS Open Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of RISC OS Open Ltd nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ABORTTRAP_SUP_H
#define ABORTTRAP_SUP_H

#include "aborttrap.h"

/* Support code */

typedef enum {
	/* The first four match the way they are encoded in the instruction */
	SHIFT_LSL,
	SHIFT_LSR,
	SHIFT_ASR,
	SHIFT_ROR,
	SHIFT_RRX,
} eshift;

static inline void branchto(abtcontext_t *ctx,uint32_t val)
{
	aborttrap_reg_write_current(ctx,15,val);
}

static inline void branchwritepc(abtcontext_t *ctx,uint32_t val)
{
	if((aborttrap_cpsr_read(ctx) & PSR_ISET) == PSR_ISET_ARM)
		branchto(ctx,val&~3);
	else
		branchto(ctx,val&~1);
} 

static inline void loadwritepc(abtcontext_t *ctx,uint32_t val)
{
	if (!loadwritepc_interworking)
	{
		branchwritepc(ctx,val);
	}
	else if(val & 1)
	{
		/* go to Thumb/ThumbEE */
		aborttrap_cpsr_write(ctx, aborttrap_cpsr_read(ctx) | PSR_ISET_THUMB); /* We shouldn't be in Jazelle, so this should set us to Thumb (or keep us in Thumb/ThumbEE) */
		branchto(ctx,val&~1);
	}
	else
	{
		/* go to ARM */
		aborttrap_cpsr_write(ctx, aborttrap_cpsr_read(ctx) & ~PSR_ISET);
		branchto(ctx,val&~3);
	}
}

static inline uint32_t pcstorevalue(abtcontext_t *ctx)
{
	uint32_t pc;
	aborttrap_reg_read_current(ctx,15,&pc);
	if (!(g_ProcessorFlags & CPUFlag_StorePCplus8))
		pc += 4;
	return pc;
}

static inline uint32_t bitcount(uint32_t bits)
{
	/* Yes, this works. */
	bits -= (bits & 0xaaaaaaaa)>>1;
	bits = (bits & 0x33333333) + ((bits>>2) & 0x33333333);
	bits += bits>>4;
	bits &= 0x0f0f0f0f;
	bits += bits>>8;
	bits += bits>>16;
	return (bits & 0xff);
}

static inline int32_t sextend(uint32_t val,uint32_t size)
{
	return ((int32_t)(val<<(32-size)))>>(32-size);
}

static inline void bttf(abtcontext_t *ctx)
{
	/* Step the PC back so it points to the next instruction
	   Doesn't cope with 32bit thumb instructions! */
	uint32_t cpsr = aborttrap_cpsr_read(ctx);
	uint32_t pc;
	aborttrap_reg_read_current(ctx, 15, &pc);
	pc -= ((cpsr & PSR_T)?6:4);
	aborttrap_reg_write_current(ctx, 15, pc);
}

static inline void decodeimmshift(uint32_t type,uint32_t imm5,eshift *srtype,uint32_t *amt)
{
	*srtype = (eshift) type;
	*amt = imm5;
	if(!imm5)
	{
		if(type == 3)
		{
			*srtype = SHIFT_RRX;
			*amt = 1;
		}
		else if(type != 0)
			*amt = 32;
	}
}

static inline uint32_t shift(uint32_t val,eshift type,uint32_t amt,bool c)
{
	switch(type)
	{
	case SHIFT_LSL:
		__asm
		{
		MOV	val,val,LSL amt
		}
		break;
	case SHIFT_LSR:
		__asm
		{
		MOV	val,val,LSR amt
		}
		break;
	case SHIFT_ASR:
		__asm
		{
		MOV	val,val,ASR amt
		}
		break;
	case SHIFT_ROR:
		__asm
		{
		MOV	val,val,ROR amt
		}
		break;
	case SHIFT_RRX:
	default:
		__asm
		{
		CMP	c,#1 // Should set carry flag correctly
		MOV	val,val,RRX
		}
		break;
	}
	return val;
}

#endif
